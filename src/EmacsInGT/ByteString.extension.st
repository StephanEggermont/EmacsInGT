Extension { #name : #ByteString }

{ #category : #'*EmacsInGT' }
ByteString >> readFromAsFile [
    "Read the contents of a file from a path specified by self."
    ^ self asFileReference contents.
]

{ #category : #'*EmacsInGT' }
ByteString >> writeToAsFile: aString [
    "Write the contents of aString to a file path specified by self."
    ^ self asFileReference ensureCreateFile; writeStreamDo: [:s | s nextPutAll: aString].
]
