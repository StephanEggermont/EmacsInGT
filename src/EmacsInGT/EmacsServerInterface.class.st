"
Interface with a running Emacs server via emacsclient or TCP.
"
Class {
	#name : #EmacsServerInterface,
	#superclass : #Object,
	#instVars : [
		'host',
		'port',
		'clientPath'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EmacsServerInterface >> clientPath: aString [
    clientPath := aString.
]

{ #category : #accessing }
EmacsServerInterface >> eval: anELispString [
    ^ self evalViaClient: anELispString.
]

{ #category : #accessing }
EmacsServerInterface >> evalViaClient: anELispString [
    "Evaluate anELispString by passing to emacsclient.
    
    NOTE: We trim spaces to get rid of the ending newline.
          Using `OSSUnixSubprocess` instead of `LibC` as it properly handles the argument.
    "
    | result |
	OSSUnixSubprocess new
	    command: clientPath;
	    arguments: {'-e'. anELispString};
	    redirectStdout;
	    runAndWaitOnExitDo: [ :process :outString  |
		    result := outString.
        ].
     ^ result trim.
]

{ #category : #accessing }
EmacsServerInterface >> initialize [
    clientPath := Eigtils loadSimpleKVData at: 'emacsclient'.
]
