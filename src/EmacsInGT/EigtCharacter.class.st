Class {
	#name : #EigtCharacter,
	#superclass : #EigtInteger,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtCharacter class >> ast: anELispCharacterNode [
    "TODO: implement native conversion"
    | inst aValue |
    aValue := EmacsServerInterface new eval: anELispCharacterNode value value.
    inst := EigtCharacter new value: aValue asNumber; ast: anELispCharacterNode.
	^ inst
]
