Class {
	#name : #ELispConsNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'leftParen',
		'car',
		'dot',
		'cdr',
		'rightParen',
		'expressions'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispConsNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitCons: self
]

{ #category : #generated }
ELispConsNode >> car [
	^ car
]

{ #category : #generated }
ELispConsNode >> car: anELispExpressionNode [
	self car notNil
		ifTrue: [ self car parent: nil ].
	car := anELispExpressionNode.
	self car notNil
		ifTrue: [ self car parent: self ]
]

{ #category : #generated }
ELispConsNode >> cdr [
	^ cdr
]

{ #category : #generated }
ELispConsNode >> cdr: anELispExpressionNode [
	self cdr notNil
		ifTrue: [ self cdr parent: nil ].
	cdr := anELispExpressionNode.
	self cdr notNil
		ifTrue: [ self cdr parent: self ]
]

{ #category : #generated }
ELispConsNode >> compositeNodeVariables [
	^ #(#expressions)
]

{ #category : #generated }
ELispConsNode >> dot [
	^ dot
]

{ #category : #generated }
ELispConsNode >> dot: aSmaCCToken [
	dot := aSmaCCToken
]

{ #category : #generated }
ELispConsNode >> expressions [
	^ expressions
]

{ #category : #generated }
ELispConsNode >> expressions: anOrderedCollection [
	self setParents: self expressions to: nil.
	expressions := anOrderedCollection.
	self setParents: self expressions to: self
]

{ #category : #'generated-initialize-release' }
ELispConsNode >> initialize [
	super initialize.
	expressions := OrderedCollection new: 2.
]

{ #category : #generated }
ELispConsNode >> leftParen [
	^ leftParen
]

{ #category : #generated }
ELispConsNode >> leftParen: aSmaCCToken [
	leftParen := aSmaCCToken
]

{ #category : #generated }
ELispConsNode >> nodeVariables [
	^ #(#car #cdr)
]

{ #category : #generated }
ELispConsNode >> rightParen [
	^ rightParen
]

{ #category : #generated }
ELispConsNode >> rightParen: aSmaCCToken [
	rightParen := aSmaCCToken
]

{ #category : #generated }
ELispConsNode >> tokenVariables [
	^ #(#leftParen #dot #rightParen)
]
