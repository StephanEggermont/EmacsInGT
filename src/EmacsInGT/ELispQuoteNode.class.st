Class {
	#name : #ELispQuoteNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'quote',
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispQuoteNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitQuote: self
]

{ #category : #generated }
ELispQuoteNode >> nodeVariables [
	^ #(#value)
]

{ #category : #generated }
ELispQuoteNode >> quote [
	^ quote
]

{ #category : #generated }
ELispQuoteNode >> quote: aSmaCCToken [
	quote := aSmaCCToken
]

{ #category : #generated }
ELispQuoteNode >> tokenVariables [
	^ #(#quote)
]

{ #category : #generated }
ELispQuoteNode >> value [
	^ value
]

{ #category : #generated }
ELispQuoteNode >> value: anELispExpressionNode [
	self value notNil
		ifTrue: [ self value parent: nil ].
	value := anELispExpressionNode.
	self value notNil
		ifTrue: [ self value parent: self ]
]
