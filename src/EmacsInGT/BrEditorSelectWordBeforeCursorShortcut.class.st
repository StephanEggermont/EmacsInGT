Class {
	#name : #BrEditorSelectWordBeforeCursorShortcut,
	#superclass : #BrEditorShortcut,
	#category : #EmacsInGT
}

{ #category : #accessing }
BrEditorSelectWordBeforeCursorShortcut >> description [
	^ 'Selects one word at a time to the left from the cursor.'
]

{ #category : #accessing }
BrEditorSelectWordBeforeCursorShortcut >> initialize [
	super initialize.
	
	combination := BlKeyCombination shiftPrimaryArrowLeft.
]

{ #category : #accessing }
BrEditorSelectWordBeforeCursorShortcut >> name [
	^ 'Select word to the left'
]

{ #category : #accessing }
BrEditorSelectWordBeforeCursorShortcut >> performOnEditor: aBrTextEditor element: aBrEditorElement dueTo: aShortcutEvent [
    | selecter startPos text endPos curPos hitNonWS |
    selecter := aBrTextEditor selecter.
    startPos := selecter cursors first position. "assume a single cursor"
    startPos = 0 ifTrue: [^ self].
    text := selecter editor text asString.
    endPos := ShortcutFactory findEndPosIn: text for: '\s' startingAt: startPos going: #left.
    endPos = nil ifTrue: [^ self].
    selecter
		withoutCursorUpdate;
		moveBy: endPos - startPos;
		select.
	aBrTextEditor navigator
		moveToStartPreviousWord;
		apply
]
