Class {
	#name : #ELispHashTableNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'prelim',
		'expressionses',
		'rightParen'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispHashTableNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitHashTable: self
]

{ #category : #generated }
ELispHashTableNode >> compositeNodeVariables [
	^ #(#expressionses)
]

{ #category : #generated }
ELispHashTableNode >> expressionses [
	^ expressionses
]

{ #category : #generated }
ELispHashTableNode >> expressionses: anOrderedCollection [
	self setParents: self expressionses to: nil.
	expressionses := anOrderedCollection.
	self setParents: self expressionses to: self
]

{ #category : #'generated-initialize-release' }
ELispHashTableNode >> initialize [
	super initialize.
	expressionses := OrderedCollection new: 2.
]

{ #category : #generated }
ELispHashTableNode >> prelim [
	^ prelim
]

{ #category : #generated }
ELispHashTableNode >> prelim: aSmaCCToken [
	prelim := aSmaCCToken
]

{ #category : #generated }
ELispHashTableNode >> rightParen [
	^ rightParen
]

{ #category : #generated }
ELispHashTableNode >> rightParen: aSmaCCToken [
	rightParen := aSmaCCToken
]

{ #category : #generated }
ELispHashTableNode >> tokenVariables [
	^ #(#prelim #rightParen)
]
