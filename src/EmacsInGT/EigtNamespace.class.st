"
TODO: Convert to composed type under EigtObject.
"
Class {
	#name : #EigtNamespace,
	#superclass : #Dictionary,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtNamespace class >> new [
    ^ super new initialize; yourself.
]

{ #category : #accessing }
EigtNamespace >> initialize [
    ""
    self at: #t put: (EigtSymbol new name: #t).
    self at: #nil put: (EigtSymbol new name: #nil)
]
