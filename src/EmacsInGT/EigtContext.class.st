"
I am the environment for an engine. I must be passed to all cons and symbols which it evaluates.
"
Class {
	#name : #EigtContext,
	#superclass : #Object,
	#instVars : [
		'globalNS',
		'localNS',
		'activeQuote',
		'emacsServerInterface',
		'namespace',
		'primitives',
		'primitiveNS',
		'engine',
		'nativePrimitives',
		'symbolValueStack'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtContext >> engine: anEigtEngine [
    engine := anEigtEngine.
]

{ #category : #accessing }
EigtContext >> evalOnServer: anELispString [
    ^ emacsServerInterface eval: anELispString.
]

{ #category : #accessing }
EigtContext >> evalWithEngine: anEigtObject [
    ^ engine eval: anEigtObject.
]

{ #category : #accessing }
EigtContext >> findMethodName: aString [
    ^ nativePrimitives at: aString ifAbsent: [nil].
]

{ #category : #accessing }
EigtContext >> findSubr: aString [
    ^ nativePrimitives at: aString ifAbsent: [nil].
]

{ #category : #accessing }
EigtContext >> global: aSymbol [
    "Return the symbol's global value."
    ^ globalNS at: aSymbol ifAbsent: [self error: 'Not yet implemented']
]

{ #category : #accessing }
EigtContext >> inNamespaceAt: aSymbol [
    "Get an object from the most recent namespace."
    ^ namespace last at: aSymbol ifAbsent: [nil].
]

{ #category : #accessing }
EigtContext >> inNamespaceAt: aSymbol put: anEigtObject [
    "Add to the most recent namespace by default."
    namespace last at: aSymbol put: anEigtObject.
]

{ #category : #accessing }
EigtContext >> inQuote [
    ^ activeQuote.
]

{ #category : #accessing }
EigtContext >> initNativePrimitives [
	| pragmas key |
	nativePrimitives := Dictionary new.
	pragmas := (PragmaCollector
		filter: [ :prg | prg selector = 'eigtSubr:' ]) reset.
	pragmas
		do: [ :prg | 
			(prg arguments first isKindOf: String)
				ifFalse: [ self error: 
				    'Argument for pragma `', 
				    prg method methodClass name, 
				    '>>#', 
				    prg method selector, 
				    ' <', 
				    prg selector asString, 
				    '>` must be a string'
				].
			key := prg arguments first
				ifEmpty: [ prg method selector asString ].
			nativePrimitives add: key -> prg method 
        ].
    ^ nil.
]

{ #category : #accessing }
EigtContext >> initPrimitives [
    "Initialize symbols for primitive functions."
    | prims |
	prims := '(let ((primitives-list ''()))
     (mapatoms
      (lambda (sym)
        (if (subrp (symbol-function sym))
           (push sym primitives-list))))
     primitives-list)'.
     primitives := ((self evalOnServer: prims) allButFirst allButLast findTokens: ' ') collect: [:p | p asSymbol].
     primitives do: [:prim | primitiveNS at: prim put: (EigtSymbol new name: prim; fset: self nil; yourself)].
]

{ #category : #accessing }
EigtContext >> initServerPrimitives [
    "Initialize symbols for primitive functions."
    | prims |
	prims := '(let ((primitives-list ''()))
     (mapatoms
      (lambda (sym)
        (if (subrp (symbol-function sym))
           (push sym primitives-list))))
     primitives-list)'.
     primitives := ((self evalOnServer: prims) allButFirst allButLast findTokens: ' ') collect: [:p | p asSymbol].
     primitives do: [:prim | primitiveNS at: prim put: (EigtSymbol new name: prim; fset: self nil; yourself)].
]

{ #category : #accessing }
EigtContext >> initialize [
    activeQuote := false.
    primitiveNS := EigtNamespace new.
    globalNS := EigtNamespace new.
    namespace := OrderedCollection new. "TODO: make linked list?"
    namespace add: primitiveNS; add: globalNS.
    symbolValueStack := LinkedList new.
    self initNativePrimitives.
    emacsServerInterface := EmacsServerInterface new.
	self initServerPrimitives.
]

{ #category : #accessing }
EigtContext >> lookup: aSymbol for: aCellTypeSymbol [
    ""
    | foundSymbol |
    (#(any functionCell valueCell) contains: [:ct | ct = aCellTypeSymbol]) 
        ifFalse: [self error: 'aCellTypeSymbol must be #functionCell, #valueCell or #any'].
	namespace reverseDo: [:ns | 
	    foundSymbol := ns at: aSymbol ifAbsent: [nil].
	    foundSymbol ifNotNil: [
	        aCellTypeSymbol = #any ifTrue: [^ foundSymbol].
	        (foundSymbol perform: aCellTypeSymbol) ifNotNil: [^ foundSymbol].
	    ].
	].
    ^ nil.
]

{ #category : #accessing }
EigtContext >> lookupFunctionFor: aSymbol [
    "Search from the most recent local namespace back to global for aSymbol."
    ^ self lookup: aSymbol for: #functionCell.
]

{ #category : #accessing }
EigtContext >> lookupValueFor: aSymbol [
    "Search from the most recent local namespace back to global for aSymbol."
    ^ self lookup: aSymbol for: #valueCell.
]

{ #category : #accessing }
EigtContext >> namespace [
    ^ namespace.
]

{ #category : #accessing }
EigtContext >> nil [
    ^ globalNS at: #nil.
]

{ #category : #accessing }
EigtContext >> popSymbolValue [
    | svp |
	svp := symbolValueStack removeLast.
	svp first set: svp second.
]

{ #category : #accessing }
EigtContext >> popSymbolValues: anInteger [
    1 to: anInteger do: [:_ | self popSymbolValue].
]

{ #category : #accessing }
EigtContext >> pushSymbolValue: anEigtSymbol [
    symbolValueStack addFirst: {anEigtSymbol. anEigtSymbol symbolValue}.
]

{ #category : #accessing }
EigtContext >> pushSymbolValueFor: anEigtSymbol [
    symbolValueStack addFirst: {anEigtSymbol. anEigtSymbol symbolValue}.
]

{ #category : #accessing }
EigtContext >> pushSymbolValues: anEigtSymbolCollection [
    anEigtSymbolCollection do: [:es | self pushSymbolValue: es].
]

{ #category : #accessing }
EigtContext >> pushSymbolValuesFor: anEigtSymbolCollection [
    anEigtSymbolCollection do: [:es | self pushSymbolValueFor: es].
]

{ #category : #accessing }
EigtContext >> quote [
    ^ activeQuote.
]

{ #category : #accessing }
EigtContext >> quote: aQuoteOrFalse [
    ^ activeQuote := aQuoteOrFalse.
]

{ #category : #accessing }
EigtContext >> quoted [
    ^ activeQuote.
]

{ #category : #accessing }
EigtContext >> quoted: aBoolean [
    ^ activeQuote := aBoolean.
]

{ #category : #accessing }
EigtContext >> readWithEngine: anELispString [
    ""
    ^ engine read: anELispString.
]

{ #category : #accessing }
EigtContext >> t [
    ^ globalNS at: #t.
]
