Class {
	#name : #ELispCharacterNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispCharacterNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitCharacter: self
]

{ #category : #generated }
ELispCharacterNode >> tokenVariables [
	^ #(#value)
]

{ #category : #generated }
ELispCharacterNode >> value [
	^ value
]

{ #category : #generated }
ELispCharacterNode >> value: aSmaCCToken [
	value := aSmaCCToken
]
