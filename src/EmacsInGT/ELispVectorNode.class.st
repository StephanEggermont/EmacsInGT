Class {
	#name : #ELispVectorNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'leftBracket',
		'expressions',
		'rightBracket'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispVectorNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitVector: self
]

{ #category : #generated }
ELispVectorNode >> compositeNodeVariables [
	^ #(#expressions)
]

{ #category : #generated }
ELispVectorNode >> expressions [
	^ expressions
]

{ #category : #generated }
ELispVectorNode >> expressions: anOrderedCollection [
	self setParents: self expressions to: nil.
	expressions := anOrderedCollection.
	self setParents: self expressions to: self
]

{ #category : #'generated-initialize-release' }
ELispVectorNode >> initialize [
	super initialize.
	expressions := OrderedCollection new: 2.
]

{ #category : #generated }
ELispVectorNode >> leftBracket [
	^ leftBracket
]

{ #category : #generated }
ELispVectorNode >> leftBracket: aSmaCCToken [
	leftBracket := aSmaCCToken
]

{ #category : #generated }
ELispVectorNode >> rightBracket [
	^ rightBracket
]

{ #category : #generated }
ELispVectorNode >> rightBracket: aSmaCCToken [
	rightBracket := aSmaCCToken
]

{ #category : #generated }
ELispVectorNode >> tokenVariables [
	^ #(#leftBracket #rightBracket)
]
