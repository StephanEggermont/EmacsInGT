Class {
	#name : #EigtEngineExamples,
	#superclass : #EigtBaseExampleClass,
	#instVars : [
		'repParams'
	],
	#category : #'EmacsInGT-Examples'
}

{ #category : #accessing }
EigtEngineExamples >> readEvalPrint: anELispInputString andExpect: anELispOutputString [
    ""
    <gtExample>
    <parametrizeUsing: #repParams>
    | result |
	self getParamsFor: #repParams from: [
        GeaParameterSets new
            paramNames: #(anELispInputString anELispOutputString);
            + #('123' '123');
            + #('''123' '123');
            + #('(eval ''123)' '123');
            + #('(+ 1 3 5)' '9');
            + #('(setq a 123)' '123');
            + #('(setq a 123) (eval ''a)' '123');
            + #('(setq a 123) a' '123');
            + #('(symbol-function ''car)' '#<subr car>');  "ans is a symbol"
            + #('(fset ''first ''car)' 'car');
            + #('(fset ''first ''car) (first ''(1 2 3))' '1');
            + #('(funcall (lambda (a b c) (+ a b c)) 1 2 3)' '6');
            + #('(funcall ''list ''x ''y ''z)' '(x y z)');
            + #('(defmacro inc (var) (list ''setq var (list ''1+ var))) (inc 2)' '3');
            + #('(if nil (print ''true) ''very-false)' 'very-false');
            + #('(setq a 5) (cond ((eq a ''hack) ''foo) (t "default"))' '"default"');
            + #('(and (print 1) (print 2) nil (print 3))' 'nil');
            + #('(setq x 1) (or (eq x nil) (eq x 0))' 'nil');
            + #('(setq y 2) (let ((y 1) (z y)) (list y z))' '(1 2)');
            + #('(setq y 2) (let* ((y 1) (z y)) (list y z))' '(1 1)');
            + #('(prog1 1 2 3)' '1');
            + #('(prog2 1 2 3)' '2');
            + #('(progn 1 2 3)' '3');
            + #('(defalias ''add ''+) (add 2 1)' '3');
            + #('#''+' '+');
            + #('(function var)' 'var');
            + #('(apply ''+ 1 2 ''(3 4))' '10');
            + #('(defmacro t-becomes-nil (variable) `(if (eq ,variable t) (setq ,variable nil))) (t-becomes-nil foo)' '(if (eq foo t) (setq foo nil))');
            + #('(defun bar (a &optional b &rest c) (list a b c)) (bar 1 2 3 4 5)' '(1 2 (3 4 5))')
            "NOTE: macro needed!"
            "+ #('(setq x 100) (defun getx () x) (let ((x 1)) (getx))' '1');
            + #('(defvar lexical-binding t) (setq x 100) (defun getx () x) (let ((x 1)) (getx))' '100')".
            
    ].
    result := EigtEngine new read: anELispInputString; eval; print.
    self assert: result = anELispOutputString description: '(eval ', anELispInputString, ')` is `', result, '`, not `', anELispOutputString, '`'.
    ^ result.
]
