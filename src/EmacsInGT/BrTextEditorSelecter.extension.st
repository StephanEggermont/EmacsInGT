Extension { #name : #BrTextEditorSelecter }

{ #category : #'*EmacsInGT' }
BrTextEditorSelecter >> moveBy: anInteger [
	"Select and move a cursor by a given number of characters, right if positive and left if negative."
	| theCursorsOnTheLeft theCursorsOnTheRight theCursorsOutside |
	self assertUIProcess.
	
	theCursorsOnTheLeft := self cursors
		select: [ :aCursor | self selection anySatisfy: [ :aSelection | aCursor position = aSelection from ] ].
		
	theCursorsOnTheRight := self cursors
		select: [ :aCursor | self selection anySatisfy: [ :aSelection | aCursor position = aSelection to ] ].
	
	theCursorsOutside := self cursors
		select: [ :aCursor | (self selection anySatisfy: [ :aSelection | aCursor position between: aSelection from and: aSelection to ]) not ].
		
	"the cursor is on the other side of the selection, we should deselect all"
	theCursorsOnTheLeft do: [ :aCursor |
		self editor deselecter"
			from: aCursor position to: aCursor position + anInteger;
			deselect" all ].
	
	"the cursor is on the correct side of the selection or is not inside any selection, we should extend it by given number of character(s) in that direction"
    anInteger >= 0
        ifTrue: [
            theCursorsOnTheRight, theCursorsOutside do: [ :aCursor | self from: aCursor position to: ((aCursor position + anInteger) min: self text size) ]
        ]
        ifFalse: [
            theCursorsOnTheLeft, theCursorsOutside do: [ :aCursor | self from: ((aCursor position - anInteger abs) max: 0) to: aCursor position ]
        ].
]
