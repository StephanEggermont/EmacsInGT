Class {
	#name : #ELispSymbolNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'name'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispSymbolNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitSymbol: self
]

{ #category : #generated }
ELispSymbolNode >> name [
	^ name
]

{ #category : #generated }
ELispSymbolNode >> name: aSmaCCToken [
	name := aSmaCCToken
]

{ #category : #generated }
ELispSymbolNode >> tokenVariables [
	^ #(#name)
]
