Class {
	#name : #EigtInteger,
	#superclass : #EigtAtom,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtInteger class >> ast: anELispIntegerNode [
    "Create an integer type."
    | inst aValue base |
    aValue := anELispIntegerNode value value trimRight: [:c | c = $.].
    inst := aValue first = $#
        ifTrue: [
            base := (Dictionary new add: $b->2; add: $o->8; add: $x->16; yourself) at: aValue second ifAbsent: [aValue allButFirst].
            base isKindOf: Integer
                ifTrue: [EigtInteger new value: (base asString, 'r', (aValue allButFirst: 2)) asNumber; ast: anELispIntegerNode]
                ifFalse: [EigtInteger new value: aValue allButFirst asNumber; ast: anELispIntegerNode]
        ]
        ifFalse: [EigtInteger new value: aValue asNumber; ast: anELispIntegerNode].
    ^ inst.
]
