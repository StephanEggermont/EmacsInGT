Class {
	#name : #ShortcutFactory,
	#superclass : #Object,
	#instVars : [
		'combo',
		'action'
	],
	#category : #EmacsInGT
}

{ #category : #accessing }
ShortcutFactory class >> findEndPosIn: text for: regex startingAt: startPos going: aDirection [
    "Find the endPos wiithin text.
    
    aDirection -- #left or #right.
    "
    | curPos hitRegex endPos oneMove lastPos |
    self
        assert: [aDirection = #left or: [aDirection = #right]] 
        description: 'aDirection must be #left or #right'.
    aDirection = #left ifTrue: [^ self findEndPosLeftIn: text for: regex startingAt: startPos going: #left].
    curPos := startPos.
    hitRegex := false.
	aDirection = #left
	    ifTrue: [oneMove := -1. lastPos := 0". startPos <= 0 ifTrue: [^ nil]"]
	    ifFalse: [oneMove := 1. lastPos := text size. startPos >= lastPos ifTrue: [^ nil]].
    ((text at: startPos) asString matchesRegex: regex) not & ((text at: (startPos + oneMove max: 1)) asString matchesRegex: regex) ifTrue: [curPos := curPos + oneMove].
    curPos to: lastPos by: oneMove do: [:idx | ((idx > 0) and: [((text at: idx) asString matchesRegex: regex) & hitRegex not]) ifTrue: [curPos := curPos + oneMove] ifFalse: [hitRegex := true]].
    endPos := (text indexEquals: [:idx | idx asString matchesRegex: regex] startingAt: curPos going: aDirection" min: text size max: 1").
    endPos = nil ifTrue: [endPos := lastPos] ifFalse: [endPos := endPos - oneMove]. "nil if signal not found; subtract 1 to avoid including signal"
    startPos = 1 ifTrue: [endPos := endPos + 1]. "fix missing last char if from start. WHY?"
    "startPos = (lastPos - 1) ifTrue: [endPos := endPos + oneMove]."
    "endPos <= 0 ifTrue: [endPos := lastPos]."
    "endPos > lastPos ifTrue: [endPos := lastPos]."
    ^ endPos.
	
]

{ #category : #accessing }
ShortcutFactory class >> findEndPosLeftIn: text for: regex startingAt: startPos going: aDirection [
    "Find the endPos wiithin text, to the left.
    
    aDirection -- #left or #right.
    TODO: merge with other version once kinks are ironed out...
    "
    | curPos hitRegex endPos oneMove lastPos subText isSame |
    self assert: [aDirection = #left or: [aDirection = #right]] description: 'aDirection must be #left or #right'.
    curPos := startPos.
    hitRegex := false.
	aDirection = #left
	    ifTrue: [oneMove := -1. lastPos := 0. subText := text copyFrom: 1 to: startPos]
	    ifFalse: [oneMove := 1. lastPos := text size. subText := text copyFrom: startPos to: lastPos. startPos >= lastPos ifTrue: [^ nil]].
	"Halt now."
	(subText allSatisfy: [:c | c asString matchesRegex: regex]) | (subText allSatisfy: [:c | (c asString matchesRegex: regex) not]) ifTrue: [^ 0]. "all signal or non-signal chars to end"
	"curPos := curPos + oneMove."
    ((text at: curPos) asString matchesRegex: regex) ifTrue: [curPos := (text indexEquals: [:c | (c asString matchesRegex: regex) not] startingAt: curPos going: aDirection) ifNil: [0]].
    "((text at: startPos) asString matchesRegex: regex) not & ((text at: (startPos + oneMove max: 1)) asString matchesRegex: regex) ifTrue: [curPos := curPos + oneMove]."
    "curPos to: lastPos by: oneMove do: [:idx | ((idx > 0) and: [((text at: idx) asString matchesRegex: regex) & hitRegex not]) ifTrue: [curPos := curPos + oneMove] ifFalse: [hitRegex := true]]."
    endPos := (text indexEquals: [:c | c asString matchesRegex: regex] startingAt: curPos going: aDirection).
    endPos = nil ifTrue: [endPos := lastPos] "ifFalse: [endPos := endPos - oneMove]". "nil if signal not found; subtract 1 to avoid including signal"
    startPos = 1 ifTrue: [endPos := endPos + 1]. "fix missing last char if from start. WHY?"
    "startPos = (lastPos - 1) ifTrue: [endPos := endPos + oneMove]."
    "endPos <= 0 ifTrue: [endPos := lastPos]."
    "endPos > lastPos ifTrue: [endPos := lastPos]."
    ^ endPos.
	
]

{ #category : #accessing }
ShortcutFactory >> addAction: aBlock [
    action := aBlock.
]

{ #category : #accessing }
ShortcutFactory >> addCombination: aBlKeyCombination [
    combo := aBlKeyCombination.
]

{ #category : #accessing }
ShortcutFactory >> build [
    | shortcut |
	shortcut := BlShortcutWithAction new
        combination: combo;
        action: action.
    combo := nil.
    action := nil.
    ^ shortcut.
]
