Class {
	#name : #EigtFloat,
	#superclass : #EigtAtom,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFloat class >> ast: anELispFloatNode [
    | aValue |
	aValue := anELispFloatNode value value.
	(aValue size > 2 and: [#(INF NaN) = aValue last: 3])
	    ifTrue:  [^ EigtFloat new value: (EmacsServerInterface new eval: aValue); ast: anELispFloatNode].  "TODO: handle INF/NaN properly"
	aValue := aValue copyReplaceAll: '+' with: ''.
	aValue first = $. or: [(aValue first: 2) = '-.'] ifTrue: [aValue := aValue copyReplaceAll: '.' with: '0.'].
    ^ EigtFloat new value: aValue asNumber asFloat; ast: anELispFloatNode.
]
