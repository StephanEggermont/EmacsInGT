Class {
	#name : #ELispRecordNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'prelim',
		'type',
		'expressionses',
		'rightParen'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispRecordNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitRecord: self
]

{ #category : #generated }
ELispRecordNode >> compositeNodeVariables [
	^ #(#expressionses)
]

{ #category : #generated }
ELispRecordNode >> expressionses [
	^ expressionses
]

{ #category : #generated }
ELispRecordNode >> expressionses: anOrderedCollection [
	self setParents: self expressionses to: nil.
	expressionses := anOrderedCollection.
	self setParents: self expressionses to: self
]

{ #category : #'generated-initialize-release' }
ELispRecordNode >> initialize [
	super initialize.
	expressionses := OrderedCollection new: 2.
]

{ #category : #generated }
ELispRecordNode >> nodeVariables [
	^ #(#type)
]

{ #category : #generated }
ELispRecordNode >> prelim [
	^ prelim
]

{ #category : #generated }
ELispRecordNode >> prelim: aSmaCCToken [
	prelim := aSmaCCToken
]

{ #category : #generated }
ELispRecordNode >> rightParen [
	^ rightParen
]

{ #category : #generated }
ELispRecordNode >> rightParen: aSmaCCToken [
	rightParen := aSmaCCToken
]

{ #category : #generated }
ELispRecordNode >> tokenVariables [
	^ #(#prelim #rightParen)
]

{ #category : #generated }
ELispRecordNode >> type [
	^ type
]

{ #category : #generated }
ELispRecordNode >> type: anELispSymbolNode [
	self type notNil
		ifTrue: [ self type parent: nil ].
	type := anELispSymbolNode.
	self type notNil
		ifTrue: [ self type parent: self ]
]
