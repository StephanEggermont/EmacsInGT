Extension { #name : #Character }

{ #category : #'*EmacsInGT' }
Character >> isEigtFirstSymbolChar [
    "Return true if a character is a valid first for an ELisp symbol."
    | validSymbolOrdinals invalidSymbolOrdinals |
    invalidSymbolOrdinals := #(32 34 35 40 59 63 91).  "space, double quote, number sign, open parens, semi-colon, question mark, open bracket"
    validSymbolOrdinals := OrderedCollection new.
    1 to: 65535 do: [:idx | 
        (invalidSymbolOrdinals contains: [:ord | idx = ord]) 
            ifFalse: [validSymbolOrdinals add: idx]].
	^ validSymbolOrdinals contains: [:ord | self asUnicode = ord].
]

{ #category : #'*EmacsInGT' }
Character >> isUnicode [
    "Return true if the character is not ASCII.
    
    Used primarily by ELispParser.
    "
    ^ self asUnicode > 127.
]
