Class {
	#name : #ELispParserExamples,
	#superclass : #EigtBaseExampleClass,
	#instVars : [
		'characterWithModifierBitsParams',
		'consParams',
		'hashTableParams',
		'recordParams',
		'vectorParams',
		'symbolParams',
		'characterParams',
		'floatParams',
		'integerParams',
		'functionParams'
	],
	#category : #'EmacsInGT-Examples'
}

{ #category : #accessing }
ELispParserExamples >> parseAndStripFileNode: aString [
    "Parse aString and return the main inner node.
    
    Only 1 outer entity Should be in aString.
    "
    | expressions |
    expressions := (ELispParser parse: aString) expressions.
	self assert: expressions size = 1 description: 'Encountered an ELispFileNode where size is not 1'.
    ^ expressions first.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsControlWithCDash [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\C-I'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsControlWithCaret [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\^I'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsMeta [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\M-A'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsMetaWithCDash [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\M-\C-b'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsMetaWithCDashFirst [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\C-\M-b'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsMetaWithOctal [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\M-\101'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsRegular [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?a'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterAsSpecial [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\('.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterFromLongUnicodeValue [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\U000000E0'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterFromMediumUnicodeValue [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\u00e0'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterFromShortUnicodeValue [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\N{U+E0}'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterFromUnicodeName [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\N{LATIN SMALL LETTER WITH A GRAVE}'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterHexCode [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\x41'. "A"
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterOctalCode [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?\101'. "A"
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterOfSimple [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?a'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterRegular [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '?a'.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCharacterWithModifierBits: anELispCharacterString [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Other-Char-Bits.html"
    | node |
    <gtExample>
    <parametrizeUsing: #characterWithModifierBitsParams>
    self getParamsFor: #characterWithModifierBitsParams from: [
        GeaParameterSets new
            paramNames: #(anELispCharacterString);
            paramValidators: {[:p | ((p isKindOf: String)) & (p first = $?)]};
            + #('?\C-\S-o');
            + #('?\H-\M-\A-x');
            + #('?\s-\C-r').
    ].
	node := self parseAndStripFileNode: anELispCharacterString.
    self assert: (node isMemberOf: ELispCharacterNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseCons: anELispConsString andAssertWith: anAssertBlock [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Cons-Cell-Type.html"
    | node |
    <gtExample>
    <parametrizeUsing: #consParams>
    self getParamsFor: #consParams from: [
        GeaParameterSets new
            paramNames: #(anELispConsString anAssertMessageString);
            "paramValidators: {[:p | ((p isKindOf: String)) & (p first = $() & (p last = $)) or: [p = 'nil']]};"
            "makeAssertMessageWith: [:idx :args | 'Assert failed for `', args first, '`'];"
            + {'(A 2 "A")'. [:n | n isMemberOf: ELispConsNode]};
            + {'()'. [:n | n isMemberOf: ELispConsNode]};
            "+ {'nil'. [:n | n isMemberOf: ELispConsNode]};"
            + {'("A ()")'. [:n | n isMemberOf: ELispConsNode]};
            + {'(A ())'. [:n | n isMemberOf: ELispConsNode]};
            + {'(A nil)'. [:n | n isMemberOf: ELispConsNode]};
            + {'((A B C))'. [:n | n isMemberOf: ELispConsNode]};
            + {'(1 . (2 . (3 . nil)))'. [:n | n isMemberOf: ELispConsNode]};
            + {'(list ''1+ var)'. [:n | n expressions size = 3]}.
    ].
    node := (ELispParser parse: anELispConsString) expressions first.
    self assert: (anAssertBlock value: node) description: 'Assert failed for `', anELispConsString, '`'.
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseFloat: anELispFloatString [
    | node |
    <gtExample>
    <parametrizeUsing: #floatParams>
    self getParamsFor: #floatParams from: [
        GeaParameterSets new
            paramNames: #(anELispFloatString);
            + #('1500.0');
            + #('.15e4');
            + #('15.0e+2');
            + #('+15e2');
            + #('+1500000e-3');
            + #('1.0e+INF');
            + #('0.0e+NaN').
    ].
	node := self parseAndStripFileNode: anELispFloatString.
    self assert: (node isMemberOf: ELispFloatNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseFunction: anELispFunctionString andAssertWith: anAssertBlock [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Anonymous-Functions.html"
    | node |
    <gtExample>
    <parametrizeUsing: #functionParams>
    self getParamsFor: #functionParams from: [
        GeaParameterSets new
            paramNames: #(anELispFunctionString anAssertMessageString);
            + {'#''(lambda (x) (* x x))'. [:n | n isMemberOf: ELispFunctionNode]};
            + {'#''some-symbol'. [:n | n isMemberOf: ELispFunctionNode]};
            + {'#''='. [:n | n isMemberOf: ELispFunctionNode]}
            .
    ].
    node := (ELispParser parse: anELispFunctionString) expressions first.
    self assert: (anAssertBlock value: node) description: 'Assert failed for `', anELispFunctionString, '`'.
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseHashTable: anELispHashTableString orShowMessage: anAssertMessageString [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Creating-Hash.html
    
    NOTE: definition conflicts with Record; need a way to exclude 'hash-table'. For now, we can detect and convert.
    "
    | node |
    <gtExample>
    <parametrizeUsing: #hashTableParams>
    self getParamsFor: #hashTableParams from: [
        GeaParameterSets new
            paramNames: #(anELispHashTableString anAssertMessageString);
            paramValidators: {[:p | ((p isKindOf: String)) & (p first = $#) & (p last = $))]};
            makeAssertMessageWith: [:idx :args | 'Assert failed for `', args first, '`'];
            + #('#s(hash-table size 30 data (key1 val1 key2 300))').
    ].
    node := (ELispParser parse: anELispHashTableString) expressions first.
    self assert: (node isMemberOf: ELispHashTableNode) description: anAssertMessageString.
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseInteger: anELispIntegerString [
    | node |
    <gtExample>
    <parametrizeUsing: #integerParams>
    self getParamsFor: #integerParams from: [
        GeaParameterSets new
            paramNames: #(anELispIntegerString);
            + #('1');
            + #('-1');
            + #('+1');
            + #('1.');
            + #('#b101100'); "44"
            + #('#o54');
            + #('#x2c');
            + #('#24r1k').
    ].
	node := self parseAndStripFileNode: anELispIntegerString.
    self assert: (node isMemberOf: ELispIntegerNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseIntegerWithDigitsOnly [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '1'.
    self assert: (node isMemberOf: ELispIntegerNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseRecord: anELispRecordString orShowMessage: anAssertMessageString [
    "Ref: "
    | node |
    <gtExample>
    <parametrizeUsing: #recordParams>
    self getParamsFor: #recordParams from: [
        GeaParameterSets new
            paramNames: #(anELispRecordString anAssertMessageString);
            paramValidators: {[:p | ((p isKindOf: String)) & (p first = $#) & (p last = $))]};
            makeAssertMessageWith: [:idx :args | 'Assert failed for `', args first, '`'];
            + #('#s(my-type 1 two ''(three) "four" [five])').
    ].
    node := (ELispParser parse: anELispRecordString) expressions first.
    self assert: (node isMemberOf: ELispRecordNode) description: anAssertMessageString.
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseString [
    | node |
    <gtExample>
	node := self parseAndStripFileNode: '"Just a simple string"'.
    self assert: (node isMemberOf: ELispStringNode).
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseSymbol: anELispSymbolString orShowMessage: anAssertMessageString [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Symbol-Type.html"
    | node |
    <gtExample>
    <parametrizeUsing: #symbolParams>
    self getParamsFor: #symbolParams from: [
        GeaParameterSets new
            paramNames: #(anELispSymbolString anAssertMessageString);
            paramValidators: {[:p | ((p isKindOf: String))]};
            makeAssertMessageWith: [:idx :args | 'Assert failed for `', args first, '`'];
            + #('foo');
            + #('FOO');
            + #('1+');
            + #('\+1');
            + #('\(*\ 1\ 2\)');
            + #('+-*/_~!@$%^&=:<>{}');
            + #('+#x2c') "NOTE: this is an interesting case. Emacs parses the first char and discards everything else.".
    ].
    node := (ELispParser parse: anELispSymbolString) expressions first.
    self assert: (node isMemberOf: ELispSymbolNode) description: anAssertMessageString.
    ^ node.
]

{ #category : #accessing }
ELispParserExamples >> parseVector: anELispVectorString orShowMessage: anAssertMessageString [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Vectors.html"
    | node |
    <gtExample>
    <parametrizeUsing: #vectorParams>
    self getParamsFor: #vectorParams from: [
        GeaParameterSets new
            paramNames: #(anELispVectorString anAssertMessageString);
            paramValidators: {[:p | ((p isKindOf: String)) & (p first = $[) & (p last = $])]};
            makeAssertMessageWith: [:idx :args | 'Assert failed for `', args first, '`'];
            + #('[1 two ''(three) "four" [five]]').
    ].
    node := (ELispParser parse: anELispVectorString) expressions first.
    self assert: (node isMemberOf: ELispVectorNode) description: anAssertMessageString.
    ^ node.
]
