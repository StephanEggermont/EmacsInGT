Class {
	#name : #EigtQuoteSG,
	#superclass : #EigtSubrGroup,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtQuoteSG class >> quote: anEigtArgsList [
    <eigtPrimitive: 'quote'>
    ^ anEigtArgsList first.
]
