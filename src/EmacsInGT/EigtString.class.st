Class {
	#name : #EigtString,
	#superclass : #EigtArray,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtString class >> ast: anELispStringNode [
    | inst |
	inst := super ast: anELispStringNode.
    inst value: anELispStringNode value value asString allButFirst allButLast.
    ^ inst.
]

{ #category : #accessing }
EigtString >> print [
    ^ '"', value, '"'.
]
