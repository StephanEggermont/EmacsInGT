Class {
	#name : #EigtFile,
	#superclass : #EigtSequence,
	#instVars : [
		'context'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFile class >> value: anObject [
    ^ self new value: anObject.
]

{ #category : #accessing }
EigtFile >> context: anEigtContext [
    context := anEigtContext.
]

{ #category : #accessing }
EigtFile >> eval [
    ^ EigtFile value: {((value collect: [:el | 
        "({EigtSymbol. EigtCons} contains: [:c | c = elObj class]) ifTrue:" el context: context.
        el eval
    ]) last)}.
]

{ #category : #accessing }
EigtFile >> print [
    "Return printable of the last item.
    
    TODO: make a proper printable string
    "
    ^ value last print.  "collect: [:elObj | elObj print]."
]

{ #category : #accessing }
EigtFile >> value: anObject [
    value := anObject.
]
