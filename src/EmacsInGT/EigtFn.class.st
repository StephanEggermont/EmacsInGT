Class {
	#name : #EigtFn,
	#superclass : #EigtCons,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFn >> eval [
    ^ value first.
]

{ #category : #accessing }
EigtFn >> print [
    ^ value first print.
]
