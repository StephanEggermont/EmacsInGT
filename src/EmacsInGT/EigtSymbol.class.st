Class {
	#name : #EigtSymbol,
	#superclass : #EigtAtom,
	#instVars : [
		'pListCell',
		'functionCell',
		'nameCell',
		'valueCell',
		'context'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtSymbol class >> ast: anELispSymbolNode [
    | inst |
	inst := super ast: anELispSymbolNode.
    inst value: anELispSymbolNode name value asSymbol.
    ^ inst.
]

{ #category : #accessing }
EigtSymbol class >> findOrCreateFromAst: aSymbolNode withContext: anEigtContext [
    "If aSymbol doesn't exist, create and add to anEigtContext.
    
    TODO: Find why suddenly symbol values have a space in them.
    "
    | aSymbol es |
    aSymbol := aSymbolNode name value trim asSymbol.
    ^ (anEigtContext lookup: aSymbol for: #any) ifNil: [
        es := EigtSymbol new 
            withName: aSymbol
            andContext: anEigtContext;
            ast: aSymbolNode;
            yourself.
        anEigtContext inNamespaceAt: aSymbol put: es.
        ^ es.
    ].
]

{ #category : #accessing }
EigtSymbol >> ast: anELispSymbolNode [
    | inst aSymbol |
    aSymbol := anELispSymbolNode name value asSymbol.
	inst := (context lookupFunctionFor: aSymbol) ifNil: [
	    "self value: aSymbol."
	    ast := anELispSymbolNode.
	    self.
	].
    ^ inst.
]

{ #category : #accessing }
EigtSymbol >> callWith: aList [
    "Attempt to resolve the symbol as a function."
    functionCell ifNil: [self error: 'Symbol''s function definition is void: ', nameCell].
    self notImplemented.
]

{ #category : #accessing }
EigtSymbol >> context [
    ^ context
]

{ #category : #accessing }
EigtSymbol >> context: anEigtContext [
    "Set the context (environment) for this symbol."
    "context ifNotNil: [self error: 'Changing symbol context is not allowed.']."
    context := anEigtContext.
]

{ #category : #accessing }
EigtSymbol >> eval [
    "Attempt to resolve the symbol to a bound value if not quoted."
    (({context t. context nil} contains: [:obj | obj = self]) or: nameCell first = $:)
        ifTrue: [^ self].
    "quote = false ifFalse: [^ self]."  "treat any quote style the same"
    valueCell ifNil: [self error: 'Symbol''s value as variable is void: ', nameCell].
    "^ valueCell isMemberOf: EigtSymbol
        ifTrue: [valueCell eval]
        ifFalse: [valueCell]."
    ^ valueCell.
]

{ #category : #accessing }
EigtSymbol >> fset: anEigtObject [
    <eigtPrimitive: 'fset'>
    ^ functionCell := anEigtObject.
]

{ #category : #accessing }
EigtSymbol >> get: aPropertyEigtObject [
    1 to: pListCell value size by: 2 do: [:idx | 
        (pListCell value at: idx) = aPropertyEigtObject 
            ifTrue: [^ pListCell value at: idx + 1]
    ].
    ^ context nil.
]

{ #category : #accessing }
EigtSymbol >> initialize [
    pListCell := EigtCons new.
]

{ #category : #accessing }
EigtSymbol >> lookup: aSymbol [
    "Search from the most recent local namespace back to global for aSymbol.
    
    TODO: add differentiator between variable and function lookup.
    "
    | nss foundSymbol |
	nss := context namespace.
	nss reverseDo: [:ns | 
	    foundSymbol := ns at: aSymbol ifAbsent: [nil]. 
	    foundSymbol ifNotNil: [^ foundSymbol].
	].
    ^ nil.
]

{ #category : #accessing }
EigtSymbol >> name: aSymbol [
    ""
    nameCell ifNotNil: [self error: 'Cannot change symbol representation'].
    value := aSymbol.
    nameCell := aSymbol asString.
]

{ #category : #accessing }
EigtSymbol >> print [
    ^ nameCell.
]

{ #category : #accessing }
EigtSymbol >> put: aValueEigtObject in: aPropertyEigtObject [
    "Add value to the property list"
    1 to: pListCell value size by: 2 do: [:idx | 
        (pListCell value at: idx) = aPropertyEigtObject 
            ifTrue: [^ pListCell value at: idx + 1 put: aValueEigtObject]
    ].
    pListCell value addFirst: aValueEigtObject.
    pListCell value addFirst: aPropertyEigtObject.
    ^ aValueEigtObject.
]

{ #category : #accessing }
EigtSymbol >> set: anEigtObject [
    "NOTE: Users are Eigt*SG>>#set, EigtContext>>popSymbolValue"
    <eigtPrimitive: 'set'>
    ^ valueCell := anEigtObject.
]

{ #category : #accessing }
EigtSymbol >> setq: anEigtArguments [
    <eigtPrimitive: 'setq'>
    | result args |
    args := anEigtArguments get.
	result := self set: args first eval.
	args size > 1
	    ifTrue: [^ args second setq: args allButFirst]
	    ifFalse: [^ result].
]

{ #category : #accessing }
EigtSymbol >> specialFormP [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Special-Forms.html"
    <eigtPrimitive: 'special-form-p'>
    (#('and' 'catch' 'cond' 'condition-case' 'defconst' 'defvar' 'function' 'if' 'interactive' 'lambda' 'let' 'let*' 'or' 'prog1' 'prog2' 'progn' 'quote' 'save-current-buffer' 'save-excursion' 'save-restriction' 'setq' 'setq-default' 'unwind-protect' 'while') 
        contains: [:sfs | sfs = nameCell]) ifTrue: [^ context t].
    ^ context nil.
]

{ #category : #accessing }
EigtSymbol >> symbolFunction [
    "TODO: bind to symbol-function"
    <eigtPrimitive: 'symbol-function'>
    ^ functionCell = context nil 
        ifTrue: [EigtSubrGroup new name: nameCell] 
        ifFalse: [functionCell].
]

{ #category : #accessing }
EigtSymbol >> symbolName [
    ""
    <eigtPrimitive: 'symbol-name'>
    ^ nameCell.
]

{ #category : #accessing }
EigtSymbol >> symbolPList [
    <eigtPrimitive: 'symbol-plist'>
    ^ pListCell.
]

{ #category : #accessing }
EigtSymbol >> symbolValue [
    <eigtPrimitive: 'symbol-value'>
    ^ valueCell.
]

{ #category : #accessing }
EigtSymbol >> withName: aSymbol andContext: anEigtContext [
    ""
    self name: aSymbol.
    context := anEigtContext.
]
