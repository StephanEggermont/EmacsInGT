"
My purpose is to wrap arguments into a single object in order to bypass the `#perform:withArguments:` numArgs check.
"
Class {
	#name : #EigtArgsList,
	#superclass : #Object,
	#instVars : [
		'arguments',
		'context',
		'numVars'
	],
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
EigtArgsList class >> createAndPut: aCollection withContext: anEigtContext [
    self notYetImplemented
]

{ #category : #accessing }
EigtArgsList >> context [
    ^ context.
]

{ #category : #accessing }
EigtArgsList >> context: anEigtContext [
    context := anEigtContext.
]

{ #category : #accessing }
EigtArgsList >> doesNotUnderstand: aMessage [
    self error: 'It seems this argument object was not unwrapped before use'.
]

{ #category : #accessing }
EigtArgsList >> fifth [
    ^arguments fifth.
]

{ #category : #accessing }
EigtArgsList >> first [
    ^ arguments first.
]

{ #category : #accessing }
EigtArgsList >> fourth [
    arguments fourth.
]

{ #category : #accessing }
EigtArgsList >> get [
    ^ arguments do: [:arg | arg context: context].
]

{ #category : #accessing }
EigtArgsList >> getVars: anEigtCons [
    "Associate arguments with variables in using function.
    
    TODO: Validate arguments against variables.
        - Handle less args for opt and rest (pad arguments with nils?).
    "
    | posVars optVars restVar optSplit restSplit point varsColl argsRead varNames marks |
	"posVars := Dictionary new.
    optVars := Dictionary new.
    restVar := nil.
    optSplit := restSplit := false."
    point := 0.
    varsColl := anEigtCons value.
    argsRead := 0.
    marks := #('&optional' '&rest').
    
    varNames := varsColl collect: [:var | var symbolName].
    varsColl do: [:var |
        (marks contains: [:mark | mark = var symbolName])
            ifTrue: [point := point + 1]
            ifFalse: [
                argsRead := argsRead + 1.
                varsColl last ~= var
                    ifTrue: [argsRead <= arguments size
                        ifTrue: [var set: (arguments at: argsRead)]
                        ifFalse: [var set: context nil]
                    ]
                    ifFalse: [argsRead > arguments size
                        ifTrue: [var set: context nil] 
                        ifFalse: [var set: EigtCons value: (arguments copyFrom: argsRead to: arguments size)]
                    ].
            ]
    ].
    
    "1 to: arguments size do: [:idx | 
        
        point = #pos ifTrue: [posVars at: (varsColl at: idx) symbolName put: (arguments at: idx)]
    ]."
]

{ #category : #accessing }
EigtArgsList >> opt [
    ^ self error: 'Not implemented'.
]

{ #category : #accessing }
EigtArgsList >> pos [ 
    ^ self error: 'Not implemented'.
]

{ #category : #accessing }
EigtArgsList >> put: args [
    self set: args.
]

{ #category : #accessing }
EigtArgsList >> rest [
    ^ self error: 'Not implemented'.
]

{ #category : #accessing }
EigtArgsList >> second [
    ^ arguments second.
]

{ #category : #accessing }
EigtArgsList >> set: aCollection [
    (aCollection isKindOf: Collection) ifFalse: [self error: 'EigtArguments only holds collections'].
    arguments := aCollection.
]

{ #category : #accessing }
EigtArgsList >> setVars: anEigtCons [
    "Associate arguments with variables in using function.
    
    TODO: Validate arguments against variables.
        - Handle less args for opt and rest (pad arguments with nils?).
    "
    | posVars optVars restVar optSplit restSplit point varsColl argsRead varNames marks markPoint |
        "posVars := Dictionary new.
    optVars := Dictionary new.
    restVar := nil.
    optSplit := restSplit := false."
    point := 0.
    varsColl := anEigtCons value.
    argsRead := 0.
    marks := #('&optional' '&rest').
    markPoint := '&pos'.
    
    "varNames := varsColl collect: [:var | var symbolName]."
    varsColl do: [:var |
        (marks contains: [:mark | mark = var symbolName])
            ifTrue: [point := point + 1. markPoint := var symbolName]
            ifFalse: [
                argsRead := argsRead + 1.
                context pushSymbolValueFor: var.
                (varsColl last = var and: [markPoint = marks second])
                    ifTrue: [argsRead > arguments size
                        ifTrue: [var set: context nil] 
                        ifFalse: [var set: EigtCons value: (arguments copyFrom: argsRead to: arguments size)]
                    ]
                    ifFalse: [argsRead <= arguments size
                        ifTrue: [var set: (arguments at: argsRead)]
                        ifFalse: [var set: context nil]
                    ].
            ]
    ].
    numVars := varsColl size - point.
    "1 to: arguments size do: [:idx | 
        
        point = #pos ifTrue: [posVars at: (varsColl at: idx) symbolName put: (arguments at: idx)]
    ]."
]

{ #category : #accessing }
EigtArgsList >> third [
    ^ arguments third.
]

{ #category : #accessing }
EigtArgsList >> unsetVars [
    context popSymbolValues: numVars.
]
