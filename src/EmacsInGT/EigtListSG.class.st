Class {
	#name : #EigtListSG,
	#superclass : #EigtSubrGroup,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtListSG class >> car: anEigtArgsList [
    <eigtSubr: 'car'>
    ^ anEigtArgsList first car.
]

{ #category : #accessing }
EigtListSG class >> cdr: anEigtArgsList [
    <eigtSubr: 'cdr'>
    ^ anEigtArgsList first cdr.
]

{ #category : #accessing }
EigtListSG class >> cons: anEigtArgsList [
    <eigtSubr: 'cons'>
    ^ EigtCons new cons: anEigtArgsList get first to: anEigtArgsList get second.
]
