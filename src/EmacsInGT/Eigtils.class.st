Class {
	#name : #Eigtils,
	#superclass : #Object,
	#category : #'EmacsInGT-Utilities'
}

{ #category : #accessing }
Eigtils class >> cond: aCollection [
    "Perform Lisp-style conditional execution."
    aCollection last first = true ifFalse: [self error: 'No default (true-condition) clause found in cond.'].
    aCollection do: [:clause | clause first value ifTrue: [^ clause second value]].
    self error: 'Something seems wrong. We shouldn''t be here...'.
]

{ #category : #accessing }
Eigtils class >> loadSimpleKVData [
    "Load a simple space-separated key-value datafile."
    | data dataDict kv |
    data := 'pharo-local/data.txt' asFileReference contents findTokens: Character lf asString.
    dataDict := Dictionary new.
    data do: [:line | kv := line findTokens: ' '. dataDict at: kv first put: (' ' join: kv allButFirst)].
    ^ dataDict.
]

{ #category : #accessing }
Eigtils class >> wrapStObject: anObject [
    "Wrap a native object into the corresponding EigtObject."
    self cond:
        {
            {[anObject isKindOf: Symbol]. [^ EigtSymbol value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: Integer]. [^ EigtInteger value: anObject]}.
            {[anObject isKindOf: Float]. [^ EigtFloat value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {[anObject isKindOf: String]. [^ EigtString value: anObject]}.
            {true. nil}
        } .
    self error: 'Unable to wrap unrecognized object: ', anObject class asString.
]
