"
NOTE: Subclasses EigtVector as the docs says it behaves similarly https://www.gnu.org/software/emacs/manual/html_node/elisp/Records.html#Records
"
Class {
	#name : #EigtRecord,
	#superclass : #EigtVector,
	#category : #'EmacsInGT-Core'
}
