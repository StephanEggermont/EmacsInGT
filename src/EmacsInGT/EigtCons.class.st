Class {
	#name : #EigtCons,
	#superclass : #EigtSequence,
	#instVars : [
		'logic',
		'context',
		'inQuote',
		'cdrCell',
		'carCell',
		'car',
		'cdr'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtCons >> car [
    "TODO: Change later..."
    (value isMemberOf: OrderedCollection) ifFalse: [self error: 'Update me now'].
    ^ value first.
]

{ #category : #accessing }
EigtCons >> cdr [
    "TODO: Change later..."
    (value isMemberOf: OrderedCollection) | (value isMemberOf: Array) ifFalse: [self error: 'Update me now'].
    ^ EigtCons new value: value allButFirst.
]

{ #category : #accessing }
EigtCons >> cons: anEigtObject to: anotherEigtObject [
    car := anEigtObject.
    cdr := anotherEigtObject.
    value := {anEigtObject. anotherEigtObject}.
    ^ self.
]

{ #category : #accessing }
EigtCons >> context: anEigtContext [
    "Set the context (environment) for this cons."
    "context ifNotNil: [self error: 'Changing cons context is not allowed.']."
    context := anEigtContext.
]

{ #category : #accessing }
EigtCons >> eval [
    "Evaluate self value.
    
    TODO: revise indirection mutating the cons
    "
    | size result unwrapped args functionSource formType tCar originalCar |
    size := value size.
    size = 0 ifTrue: [^ self].
    args := #().
    originalCar := tCar := value first.
    tCar context ifNil: [tCar context: context].
    [(tCar isEigtSymbol) and: [tCar symbolFunction class = EigtSymbol]] 
        "TODO: Maybe extract if it's a lambda as well"
        whileTrue: [value at: 1 put: (tCar := tCar symbolFunction)].  "handle indirection"
    (tCar isEigtSymbol and: ([tCar symbolFunction isEigtMacro] on: MessageNotUnderstood do: [false]))
        ifTrue: [^ self evalMacro].
    tCar specialFormP = context t ifTrue: [^ self evalSpecialForm].
    (tCar isEigtLambda or: [tCar isEigtSubr]) & (tCar specialFormP = context nil)
            ifTrue: [^ self evalFunction].
    value at: 1 put: (tCar := originalCar).
    self error: 'We shouldn''t be here. Rest is broken'.
    functionSource := context lookupFunctionFor: value first. "size = 1 ifTrue: [self] ifFalse: [value second]."
    size >= 3 ifTrue: [
        "TODO: attempt to resolve the operator symbol..."
        args := value allButFirst: 2.
        result := EigtSubrGroup perform: unwrapped first withArguments: {unwrapped allButFirst "NOTE: ^1"}.
    ].
    ^ result.
]

{ #category : #accessing }
EigtCons >> evalFunction [
    "Evaluate a function form."
    | args function symName spacer subr |
	args := value allButFirst collect: [:el | (el context: context; eval) context: context].
	function := value first.
    function isEigtSubr ifTrue: [
        "Primitive call."
        symName := function print.
        subr := context findSubr: symName.
        subr ifNotNil: [
            (subr methodClass instanceSide respondsTo: subr selector) ifFalse: [self error: 'Need to set `', subr methodClass instanceSide name, '>>#', subr selector, '` to class instead of instance'].
            ^ subr methodClass instanceSide
                perform: subr selector 
                withArguments: {EigtArgsList new put: args; context:context; yourself}
        ].
        spacer := value size > 1 ifTrue: [' '''] ifFalse: [''].
        ^ (context readWithEngine: (context evalOnServer: '(', symName, spacer, ((' ''' join: (args collect: [:el | el print]))), ')')) value first.
    ].
    function isEigtLambda ifTrue: [
        "Lambda call
        
        TODO: access `funcall` via context so it isn't broken by restructuring.
        "
        ^ EigtFunctionSG funcall: 
            (EigtArgsList new put: ((args addFirst: function; yourself))).
    ].
]

{ #category : #accessing }
EigtCons >> evalMacro [
    ""
    | applyOp args result |
	applyOp := context findSubr: 'apply'.
    args := OrderedCollection new
        add: value first symbolFunction cdr;
        addAll: value allButFirst;
        yourself.
    result := applyOp methodClass instanceSide
        perform: #apply
        withArguments: {EigtArgsList new put: args; context:context; yourself}.
    ^ result.
]

{ #category : #accessing }
EigtCons >> evalSpecialForm [
    ""
    | aName result subr |
	aName := value first symbolName.
    subr := (context findSubr: aName) ifNil: [self error: 'Unable to find subr: ', aName].
    (subr methodClass instanceSide respondsTo: subr selector) ifFalse: [self error: 'Need to set `', subr methodClass instanceSide name, '>>#', subr selector, '` to class instead of instance'].
    result := subr methodClass instanceSide
        perform: subr selector  "(aName, ':') asSymbol "
        withArguments: {EigtArgsList new put: value allButFirst; context:context; yourself}.
    ^ result.
]

{ #category : #accessing }
EigtCons >> evalWith: anEigtContext [
    context := anEigtContext.
    ^ self eval.
]

{ #category : #accessing }
EigtCons >> getFormType: anEigtSymbol [
    "Return one of: #function, #macro, #special or #extension."
    | aName |
	aName := anEigtSymbol symbolName.
	anEigtSymbol symbolFunction = context nil ifTrue: [^ #function].
	aName = 'macro' ifTrue: [^ #macro].
]

{ #category : #accessing }
EigtCons >> print [
    "TODO: handle dotted pair, quotes, etc"
    ^ '(', (' ' join: (value collect: [:el | el print])), ')'.
]
