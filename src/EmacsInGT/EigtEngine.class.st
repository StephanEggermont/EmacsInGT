"
I provide the primary entrypoint into using Emacs in GToolkit.
"
Class {
	#name : #EigtEngine,
	#superclass : #Object,
	#instVars : [
		'globalNS',
		'inQuote',
		'cachedExpr',
		'cachedResult',
		'context',
		'transpiler'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtEngine >> eval [
    "Evaluate a cached EigtObject.
    
    NOTE: This is a convenience method to enable chaining from #read:.
    "
    | result |
	result := self eval: nil.
	cachedExpr := nil.
	^ result.
]

{ #category : #accessing }
EigtEngine >> eval: anEigtObject [
    "Evaluate anEigtObject and return the result."
    | elisp |
	anEigtObject isNil
        ifTrue: [
            cachedExpr isNil ifTrue: [self error: 'No expression node provided.'].
        elisp := cachedExpr.
        ]
        ifFalse: [elisp := anEigtObject].
    elisp context: context.
    ^ cachedResult := elisp eval.
]

{ #category : #accessing }
EigtEngine >> initialize [
    context := EigtContext new.
    transpiler := EigtTranspiler new.
    transpiler context: context.
    context engine: self.
    "self loadLisp."
]

{ #category : #accessing }
EigtEngine >> loadLisp [
    "TODO: Eval multiple files in proper order."
    | lispDirs elFile |
	lispDirs := Eigtils loadSimpleKVData at: #lispDirs.
    elFile := self readPath: lispDirs.
    elFile context: context; eval.
]

{ #category : #accessing }
EigtEngine >> print [
    ^ cachedResult print
]

{ #category : #accessing }
EigtEngine >> read: aString [
    "Read aString and convert to anELispExpressionNode or anELispObject."
    | node |
    context ifNil: [self error: 'Failed to create context'].
    node := ELispParser parse: aString.
    cachedExpr := transpiler acceptNode: node. "NOTE: always an EigtFile"
    ^ cachedExpr.
]

{ #category : #accessing }
EigtEngine >> readFileOrFolder: aFileReference [
    ^ Eigtils cond: {
	    {[aFileReference isFile]. [{aFileReference}]}.
	    {
	        [aFileReference isDirectory]. 
	        [aFileReference allFiles select: [:file | file fullName endsWith: '.el']]
	    }.
        {true. [self error: 'Invalid file type.']}
    }.
]

{ #category : #accessing }
EigtEngine >> readPath: aStringOrCollection [
    "Read ELisp file(s) or all ELisp files recursively if given folders."
    | elFiles elFileContents path |
	elFiles := OrderedCollection new.
	elFileContents := OrderedCollection new.
	Eigtils cond: {
	    {[aStringOrCollection isKindOf: String]. [
	        path := aStringOrCollection asFileReference.
	        elFiles := self readFileOrFolder: path.
	    ]}.
	    {[aStringOrCollection isKindOf: Collection]. [
	        aStringOrCollection do: [:aPath |
	            elFiles addAll: self readFileOrFolder: aPath asFileReference.
	        ]
	    ]}.
	    {true. [self error: 'A String or Collection was expected, but got a ', aStringOrCollection class asString]}
	}.
	elFileContents := elFiles collect: [:file | file contents].
	^ EigtFile value: (elFileContents collect: [:content | self read: content]).
]
