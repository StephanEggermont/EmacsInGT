"
I am a convenience class to ease the nesting of quote expressions. Not sure if I should be in the core...
"
Class {
	#name : #EigtQuote,
	#superclass : #EigtCons,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtQuote >> eval [
    ^ value.
]

{ #category : #accessing }
EigtQuote >> print [
    ^ quote, value print.
]
