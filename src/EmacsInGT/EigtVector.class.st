Class {
	#name : #EigtVector,
	#superclass : #EigtArray,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtVector >> ast: anELispRecordNode [
    | inst aValue |
    aValue := anELispRecordNode value value.
    inst := EigtRecord new value: aValue; ast: anELispRecordNode.
	^ inst
]
