"
Primitive operations.

NOTE ^1: There is a strange check in `Object >> #perform:withArguments:inSuperClass:` for `selector numArgs = argArray size`. This breaks the overloading of non-alphabetic symbols accepting 2+ size collections. It is bypassed by wrapping in another collection
"
Class {
	#name : #EigtSubrGroup,
	#superclass : #Object,
	#instVars : [
		'primitives',
		'meta'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtSubrGroup class >> _doesNotUnderstand: aMessage [
    | coll binaryOperators |
    binaryOperators := '[~\-!@%&*+=\\|?/><,]+'.
    (aMessage selector asString matchesRegex: binaryOperators)
        ifTrue: [
            coll := OrderedCollection new add: aMessage selector; addAll: aMessage arguments first "NOTE: ^1"; yourself.
            ^ self doNativeBinaryOperation: coll].
]

{ #category : #accessing }
EigtSubrGroup class >> doNativeBinaryOperation: aList [
    "Perform a natively binary operation on an arbitrary number of operands."
    | accum message |
	aList size >= 3 ifFalse: [^ self error: 'Binary operation requires at lease 3 items.'].
    accum := aList second.
    message := aList first.
    3 to: aList size do: [ :idx | accum := accum perform: message withArguments: {(aList at: idx)} ].
    ^ accum.
]

{ #category : #accessing }
EigtSubrGroup class >> doesNotUnderstand: aMessage [
    | coll binaryOperators |
    binaryOperators := '[~\-!@%&*+=\\|?/><,]+'.
    (aMessage selector asString matchesRegex: binaryOperators)
        ifTrue: [
            coll := OrderedCollection new add: aMessage selector; addAll: aMessage arguments first "NOTE: ^1"; yourself.
            ^ self doNativeBinaryOperation: coll].
]

{ #category : #accessing }
EigtSubrGroup class >> resolve: anEigtObject [
    
]

{ #category : #accessing }
EigtSubrGroup class >> sendToNative: aList [
    "Perform a Pharo-native operation."
    | receiver message args |
	aList size >= 2 ifFalse: [^ self error: 'A native call requires at lease 2 items.'].
    receiver := aList first.
    message := aList second.
    args := aList allButFirst: 2.
    ^ receiver perform: message withArguments: args.
]

{ #category : #accessing }
EigtSubrGroup >> initialize [
    meta := Dictionary new.
]

{ #category : #accessing }
EigtSubrGroup >> name: aString [
    meta at: 'name' put: aString.
]

{ #category : #accessing }
EigtSubrGroup >> print [
    ^ '#<subr ', (meta at: 'name'), '>'.
]
