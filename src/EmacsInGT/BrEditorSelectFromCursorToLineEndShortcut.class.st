Class {
	#name : #BrEditorSelectFromCursorToLineEndShortcut,
	#superclass : #BrEditorShortcut,
	#category : #EmacsInGT
}

{ #category : #accessing }
BrEditorSelectFromCursorToLineEndShortcut >> description [
	^ 'Selects from the cursor to the end of the line.'
]

{ #category : #accessing }
BrEditorSelectFromCursorToLineEndShortcut >> initialize [
	super initialize.
	
	combination := BlKeyCombination shiftEnd.
]

{ #category : #accessing }
BrEditorSelectFromCursorToLineEndShortcut >> name [
	^ 'Select to line end'
]

{ #category : #accessing }
BrEditorSelectFromCursorToLineEndShortcut >> performOnEditor: aBrTextEditor element: aBrEditorElement dueTo: aShortcutEvent [
    | selecter startPos text endPos curPos hitNonWS |
    selecter := aBrTextEditor selecter.
    startPos := selecter cursors first position max: 1. "assume a single cursor"
    text := selecter editor text asString.
    startPos >= text size ifTrue: [^ self].
    endPos := ShortcutFactory findEndPosIn: text for: '\n|\r' replaceEscapeSequences startingAt: startPos going: #right.
    selecter
		withoutCursorUpdate;
		moveBy: endPos - startPos;
	    select.
	aBrTextEditor navigator
		moveToLineEnd;
		apply
]
