Class {
	#name : #EigtFunctionSG,
	#superclass : #EigtSubrGroup,
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtFunctionSG class >> apply: anEigtArgsList [
    <eigtSubr: 'apply'>
    | args |
	anEigtArgsList get last class = EigtCons ifFalse: [self error: 'Wrong type of argument: listp, ', anEigtArgsList get last print].
    args := anEigtArgsList get allButLast.
    args addAll: anEigtArgsList get last value.
    ^ self funcall: (EigtArgsList new put: args; context: anEigtArgsList context; yourself).
]

{ #category : #accessing }
EigtFunctionSG class >> closure: anEigtArgsList [
    <eigtSubr: ''>
    self notYetImplemented
]

{ #category : #accessing }
EigtFunctionSG class >> defalias: anEigtArgsList [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Defining-Functions.html
    
    TODO: Account for `defalias-fset-function` property.
        - Handle doc.
    "
    <eigtSubr: 'defalias'>
    anEigtArgsList first fset: anEigtArgsList second.
    ^ anEigtArgsList context t.
]

{ #category : #accessing }
EigtFunctionSG class >> eval: anEigtArgsList [
    "TODO: Add optional lexical handling"
    <eigtSubr: 'eval'>
    | eo |
	eo := anEigtArgsList first.
    ^ eo context evalWithEngine: eo.
]

{ #category : #accessing }
EigtFunctionSG class >> funcall: anEigtArgsList [
    <eigtSubr: 'funcall'>
    | args function context pos opt rest funcArgs result newArgs |
	args := anEigtArgsList get.
	function := args first.
	function class = EigtLambda ifTrue: [
	    function := function value.
	    funcArgs := EigtArgsList new put: args allButFirst; context: args second context; yourself.
	    funcArgs setVars: function second.
	    (function allButFirst: 2) do: [:form | result := form eval].  "eval the lambda forms"
	    funcArgs unsetVars.
	] ifFalse: [
	    "TODO: Signal on macro or special form."
	    newArgs := OrderedCollection new "no need to quote the function"
	        add: args first;
	        addAll: (args allButFirst collect: [:arg | EigtQuote new value: arg]);
            yourself.
	    result := EigtCons new 
	        value: newArgs;
	        context: anEigtArgsList context;
	        eval.
	].
	^ result.
]

{ #category : #accessing }
EigtFunctionSG class >> function: anEigtArgsList [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Anonymous-Functions.html
    
    TODO: Convert arg to closure if lexical is active.
    "
    <eigtSubr: 'function'>
    ^ anEigtArgsList first.
]

{ #category : #accessing }
EigtFunctionSG class >> lambda: anEigtArgsList [
    "NOTE: We replace the car which got removed due to function eval, so index refs line up.
    TODO: Replace with macro.
    "
    <eigtSubr: 'lambda'>
    ^ EigtLambda value: (anEigtArgsList get addFirst: 'lambda-symbol-marker'; yourself).
]
