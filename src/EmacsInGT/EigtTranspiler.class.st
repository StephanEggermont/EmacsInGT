Class {
	#name : #EigtTranspiler,
	#superclass : #ELispExpressionNodeVisitor,
	#instVars : [
		'cachedExprNode',
		'namespace',
		'quoted',
		'inQuote',
		'context'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #accessing }
EigtTranspiler >> acceptNodes: aCollection [
	^ aCollection collect: [ :each | self acceptNode: each ].
]

{ #category : #accessing }
EigtTranspiler >> context: anEigtContext [
    context := anEigtContext.
]

{ #category : #accessing }
EigtTranspiler >> eval [
    "Evaluate a cached node.
    
    NOTE: This is a convenience method to enable chaining from #read:.
    "
    | result |
	result := self eval: nil.
	cachedExprNode := nil.
	^ result.
]

{ #category : #accessing }
EigtTranspiler >> eval: anELispExpressionNode [
    "Evaluate anELispExpressionNode and return the result."
    | sexp |
    anELispExpressionNode isNil
        ifTrue: [
            cachedExprNode isNil ifTrue: [self error: 'No expression node provided.'].
        sexp := cachedExprNode.
        ]
        ifFalse: [sexp := anELispExpressionNode].
    ^ self acceptNode: sexp.
]

{ #category : #accessing }
EigtTranspiler >> initialize [
    namespace := EigtNamespace new.
]

{ #category : #accessing }
EigtTranspiler >> read: aString [
    "Read aString and convert to anELispExpressionNode or anELispObject."
    ^ cachedExprNode := ELispParser parse: aString.
]

{ #category : #accessing }
EigtTranspiler >> visitCharacter: aCharacter [
    "Store and behave as integer, show as char"
    ^ EigtCharacter ast: aCharacter.
]

{ #category : #accessing }
EigtTranspiler >> visitCons: aCons [
    | inst value |
    inst := EigtCons ast: aCons.
	value := self visitExpression: aCons. "NOTE: do this here since there are sub-nodes"
	inst value: value.
	inst quote: context quote.
	inst context: context.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitFile: aFile [
    | inst value |
	inst := EigtFile ast: aFile.
	value := self visitExpression: aFile. "NOTE: do this here since there are sub-nodes"
	inst value: value.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitFloat: aFloat [
    ^ EigtFloat ast: aFloat.
]

{ #category : #accessing }
EigtTranspiler >> visitFunction: aFunction [
    | inst value |
    inst := EigtFn ast: aFunction.
	value := self visitExpression: aFunction.
	inst value: value.
	inst quote: context quote.
	inst context: context.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitInteger: anInteger [
    ^ EigtInteger ast: anInteger.
]

{ #category : #accessing }
EigtTranspiler >> visitQuote: aQuote [
    "Handle the quote and backquote shortcuts."
    | value inst |
    inst := EigtQuote ast: aQuote.
	context quote: aQuote quote value.
    value := self visitExpression: aQuote.
    context quote: false.
    inst value: value first.
    inst quote: aQuote quote value.
    ^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitRecord: aRecordOrHashTable [
    | inst value |
	value := self visitExpression: aRecordOrHashTable.
	inst := (value first name = 'hash-table' ifTrue: [EigtHashTable] ifFalse: [EigtRecord]) ast: aRecordOrHashTable.
	inst value: value.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitSmaCCParseNode: aSmaCCParseNode [
	^ self acceptNodes: aSmaCCParseNode sortedChildren.
]

{ #category : #accessing }
EigtTranspiler >> visitString: aString [
    ^ EigtString ast: aString.
]

{ #category : #accessing }
EigtTranspiler >> visitSymbol: aSymbol [
    | inst |
	inst := EigtSymbol findOrCreateFromAst: aSymbol withContext: context.
	inst quote: context quote.
	^ inst.
]

{ #category : #accessing }
EigtTranspiler >> visitVector: aVector [
    | inst value |
	inst := EigtVector ast: aVector.
	value := self visitExpression: aVector.
	inst value: value.
	^ inst.
]
