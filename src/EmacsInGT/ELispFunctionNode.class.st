Class {
	#name : #ELispFunctionNode,
	#superclass : #ELispExpressionNode,
	#instVars : [
		'hashQuote',
		'value'
	],
	#category : #'EmacsInGT-AST'
}

{ #category : #generated }
ELispFunctionNode >> acceptVisitor: anExpressionVisitor [
	^ anExpressionVisitor visitFunction: self
]

{ #category : #generated }
ELispFunctionNode >> hashQuote [
	^ hashQuote
]

{ #category : #generated }
ELispFunctionNode >> hashQuote: aSmaCCToken [
	hashQuote := aSmaCCToken
]

{ #category : #generated }
ELispFunctionNode >> nodeVariables [
	^ #(#value)
]

{ #category : #generated }
ELispFunctionNode >> tokenVariables [
	^ #(#hashQuote)
]

{ #category : #generated }
ELispFunctionNode >> value [
	^ value
]

{ #category : #generated }
ELispFunctionNode >> value: anELispExpressionNode [
	self value notNil
		ifTrue: [ self value parent: nil ].
	value := anELispExpressionNode.
	self value notNil
		ifTrue: [ self value parent: self ]
]
