"
I am the root for ELisp objects.
"
Class {
	#name : #EigtObject,
	#superclass : #Object,
	#instVars : [
		'ast',
		'value',
		'meta',
		'quote',
		'context'
	],
	#category : #'EmacsInGT-Core'
}

{ #category : #accessing }
EigtObject class >> ast: anELispExpressionNode [
    "Initialize with the given AST node."
    | inst |
	inst := self new.
    inst ast: anELispExpressionNode.
    ^ inst.
]

{ #category : #accessing }
EigtObject class >> value: anObject [
    ^ self new value: anObject.
]

{ #category : #accessing }
EigtObject >> ast: anELispExpressionNode [
    "
    
    NOTE: This should be overridden by subclasses to properly collect the node value.
    "
    ast := anELispExpressionNode.
]

{ #category : #accessing }
EigtObject >> context [
    ^ context.
]

{ #category : #accessing }
EigtObject >> context: anEigtContext [
    context ifNil: [context := anEigtContext].
]

{ #category : #accessing }
EigtObject >> error: aMessageString [
    ^ EigtError signal: aMessageString.
]

{ #category : #accessing }
EigtObject >> eval [
    "Return self.
    
    NOTE: Self-evaluation is the default since only lists and symbols do anything else.
    "
    ^ self.
]

{ #category : #accessing }
EigtObject >> initialize [
    meta := Dictionary new.
]

{ #category : #accessing }
EigtObject >> isEigtBoolVector [
    ^ self class = EigtBoolVector.
]

{ #category : #accessing }
EigtObject >> isEigtCharTable [
    ^ self class = EigtCharTable.
]

{ #category : #accessing }
EigtObject >> isEigtCharacter [
    ^ self class = EigtCharacter.
]

{ #category : #accessing }
EigtObject >> isEigtCons [
    ^ self class = EigtCons.
]

{ #category : #accessing }
EigtObject >> isEigtFloat [
    ^ self class = EigtFloat.
]

{ #category : #accessing }
EigtObject >> isEigtHashTable [
    ^ self class = EigtHashTable.
]

{ #category : #accessing }
EigtObject >> isEigtInteger [
    ^ self class = EigtInteger.
]

{ #category : #accessing }
EigtObject >> isEigtLambda [
    "NOTE: Just a cons with the proper signature."
    ^ (self isEigtCons & (value size >= 3) and: [value first class = EigtSymbol and: [value first symbolName = 'lambda']]).
]

{ #category : #accessing }
EigtObject >> isEigtMacro [
    "NOTE: Just a cons with the proper signature."
    ^ [
        (self isEigtCons & (value size = 2) and: [value first class = EigtSymbol and: [value first symbolName = 'macro']]) and: [value second isEigtLambda].
    ] on: Error do: [false].
]

{ #category : #accessing }
EigtObject >> isEigtRecord [
    ^ self class = EigtRecord.
]

{ #category : #accessing }
EigtObject >> isEigtString [
    ^ self class = EigtString.
]

{ #category : #accessing }
EigtObject >> isEigtSubr [
    "NOTE: A symbol with a function cell containing the nil symbol."
    ^ (self isEigtSymbol) and: [self symbolFunction class = EigtSubrGroup].
]

{ #category : #accessing }
EigtObject >> isEigtSymbol [
    ^ self class = EigtSymbol.
]

{ #category : #accessing }
EigtObject >> isEigtVector [
    ^ self class = EigtVector.
]

{ #category : #accessing }
EigtObject >> macrop [
    ""
    <eigtPrimitive: ''>
    self notImplemented
]

{ #category : #accessing }
EigtObject >> notImplemented [
    ^ self notYetImplemented
]

{ #category : #accessing }
EigtObject >> print [
    "Return the printable value."
    ^ value asString.
]

{ #category : #accessing }
EigtObject >> printType [
    ^ EigtSymbol new name: (self class name allButFirst: 4) translateToLowercase.
]

{ #category : #accessing }
EigtObject >> quote: aString [
    quote := aString.
]

{ #category : #accessing }
EigtObject >> specialFormP [
    "Ref: https://www.gnu.org/software/emacs/manual/html_node/elisp/Special-Forms.html"
    <eigtPrimitive: 'special-form-p'>
    (self symbolp = context t and: [#('and' 'catch' 'cond' 'condition-case' 'defconst' 'defvar' 'function' 'if' 'interactive' 'lambda' 'let' 'let*' 'or' 'prog1' 'prog2' 'progn' 'quote' 'save-current-buffer' 'save-excursion' 'save-restriction' 'setq' 'setq-default' 'unwind-protect' 'while') 
        contains: [:sfs | sfs = self symbolName]]) ifTrue: [^ context t].
    ^ context nil.
]

{ #category : #accessing }
EigtObject >> symbolp [
    <eigtPrimitive: ''>
    ^ ((self isMemberOf: EigtSymbol)) ifTrue: [context t] ifFalse: [context nil].
]

{ #category : #accessing }
EigtObject >> typeOf [
    <eigtPrimitive: 'type-of'>
    ^ self printType
]

{ #category : #accessing }
EigtObject >> value [
    ^ value.
]

{ #category : #accessing }
EigtObject >> value: anObject [
    "Store a value who's type is representative of the object's class."
    value = nil
        ifTrue: [value := anObject]
        ifFalse: [self error: 'Value is write once.'].
]
