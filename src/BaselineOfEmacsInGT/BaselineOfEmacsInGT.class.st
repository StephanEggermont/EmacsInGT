Class {
	#name : #BaselineOfEmacsInGT,
	#superclass : #BaselineOf,
	#category : #BaselineOfEmacsInGT
}

{ #category : #accessing }
BaselineOfEmacsInGT >> baseline: spec [
    <baseline>
    spec
        baseline: 'GtExemplifierAdditions'
        with: [spec repository: 'gitlab://skeledrew/GtExemplifierAdditions:master/src'].
    spec
        baseline: 'OSSubprocess'
        with: [spec repository: 'github://pharo-contributions/OSSubprocess/repository'].
    spec for: #common do: [
        spec
            package: 'EmacsInGT';
            package: 'GtExemplifierAdditions';
            package: 'OSSubprocess'.
    ]
]
